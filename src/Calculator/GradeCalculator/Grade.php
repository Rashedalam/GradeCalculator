<?php
/**
 * Created by PhpStorm.
 * User: Rashed Alam
 * Date: 3/26/2018
 * Time: 1:23 PM
 */

namespace Pondit\Calculator\GradeCalculator;


class Grade
{
    function get_gpa($subject, $number)
    {
        if ($number >= 80)
            echo $gpa = "GPA of $subject:5(A+)";

        elseif ($number < 80 && $number >= 70)
            echo $gpa = "GPA of $subject:4(A)";

        elseif ($number < 70 && $number >= 60)
            echo $gpa = "GPA of $subject:3.50(A-)";

        elseif ($number < 60 && $number >= 50)
            echo $gpa = "GPA of $subject:3(B)";

        elseif ($number < 50 && $number >= 40)
            echo $gpa = "GPA of $subject:2(C)";

        else echo $gpa = "Failed";
        return $gpa;
    }


}